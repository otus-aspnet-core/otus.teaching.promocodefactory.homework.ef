﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceShortResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }


        public PreferenceShortResponse()
        {
        }

        public PreferenceShortResponse(Preference preference)
        {
            if (preference == null)
            {
                return;
            }

            this.Id = preference.Id;
            this.Name = preference.Name;
        }
    }
}