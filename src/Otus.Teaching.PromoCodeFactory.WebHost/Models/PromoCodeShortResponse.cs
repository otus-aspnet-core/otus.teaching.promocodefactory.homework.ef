﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Common;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PromoCodeShortResponse
    {
        public Guid Id { get; set; }

        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }

        public string PartnerName { get; set; }
        

        public PromoCodeShortResponse(PromoCode promoCode)
        {
            if (promoCode == null)
            {
                return;
            }

            Id = promoCode.Id;
            Code = promoCode.Code;
            ServiceInfo = promoCode.ServiceInfo;
            BeginDate = FormattedData.ToString(promoCode.BeginDate);
            EndDate = FormattedData.ToString(promoCode.EndDate);
            PartnerName = promoCode.PartnerName;
        }
    }
}