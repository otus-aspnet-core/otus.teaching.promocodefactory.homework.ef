﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        
        public int Age { get; set; }
        
        //TODO: Добавить список предпочтений
        public List<PreferenceShortResponse> Preferences { get; set; }
        public List<PromoCodeShortResponse> PromoCodes { get; set; }
        

        public CustomerResponse(Customer customer)
        {
            if (customer == null)
            {
                return;
            }

            this.Id = customer.Id;
            this.FirstName = customer.FirstName;
            this.LastName = customer.LastName;
            this.Email = customer.Email;
            this.Age = customer.Age;
        }

        public CustomerResponse SetPreferences(ICollection<CustomerPreference> customerPreferences)
        {
            this.Preferences = customerPreferences?
                .Where(x => x.Preference != null)
                .Select(x => new PreferenceShortResponse(x.Preference))
                .ToList() ?? this.Preferences;

            return this;
        }

        public CustomerResponse SetPromoCodes(IEnumerable<PromoCode> promoCodes)
        {
            this.PromoCodes = promoCodes?
                .Select(x => new PromoCodeShortResponse(x))
                .ToList() ?? this.PromoCodes;
            
            return this;
        }
    }
}