﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class GivePromoCodeRequest
    {
        public string ServiceInfo { get; set; }

        public string PartnerName { get; set; }

        public string PromoCode { get; set; }

        public string Preference { get; set; }

        public PromoCode ToPromoCode(Preference preference)
        {
            return new PromoCode()
            {
                Id = Guid.NewGuid(),
                Code = this.PromoCode,
                ServiceInfo = this.ServiceInfo,
                PartnerName = this.PartnerName,
                Preference = preference
            };
        }
    }
}