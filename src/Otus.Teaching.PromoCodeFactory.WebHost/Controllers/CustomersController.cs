﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(
            IRepository<Customer> customerRepository,
            IRepository<PromoCode> promoCodeRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync().ConfigureAwait(false);
            if (customers?.Any() == true)
            {
                return customers
                    .Select(x => new CustomerShortResponse(x))
                    .ToArray();
            }

            return NotFound();
        }
        
        /// <summary>
        /// Добавить получение клиента вместе с выданными ему промомкодами
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            // Вынес бы в отдельный метод репозитория (или UseCase), но тогда не получится пользоваться здесь generic-EfRepository, как указано в ТЗ
            var customer = await (await _customerRepository.GetAllAsync().ConfigureAwait(false))
                .AsQueryable()
                .Include(x => x.CustomerPreferences)
                    .ThenInclude(x => x.Preference)
                .Include(x => x.PromoCodes)
                .FirstOrDefaultAsync(x => x.Id == id)
                .ConfigureAwait(false);
            if (customer != null)
            {
                return new CustomerResponse(customer)
                    .SetPreferences(customer.CustomerPreferences)
                    .SetPromoCodes(customer.PromoCodes);
            }

            return NotFound();
        }

        /// <summary>
        /// Добавить создание нового клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="request">Запрос на создание</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            if (request != null)
            {
                var preferenceIds = request.PreferenceIds.AsEnumerable() ?? Array.Empty<Guid>();
                
                var preferences = await (await _preferenceRepository.GetAllAsync().ConfigureAwait(false))
                    .AsQueryable()
                    .Where(x => preferenceIds.Contains(x.Id))
                    .ToArrayAsync()
                    .ConfigureAwait(false);

                var customer = request
                    .ToCustomer()
                    .SetNewId()
                    .SetPreferences(preferences);

                var newCustomer = await _customerRepository.CreateAsync(customer).ConfigureAwait(false);
                if (newCustomer != null)
                {
                    return Ok();
                }
            }

            return NotFound();
        }

        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <param name="request">Запрос на обновление</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (request != null)
            {
                var preferenceIds = request.PreferenceIds.AsEnumerable() ?? Array.Empty<Guid>();
                
                var preferences = await (await _preferenceRepository.GetAllAsync().ConfigureAwait(false))
                    .AsQueryable()
                    .Where(x => preferenceIds.Contains(x.Id))
                    .ToArrayAsync()
                    .ConfigureAwait(false);

                var customer = request
                    .ToCustomer()
                    .SetNewId(id)
                    .SetPreferences(preferences);

                var newCustomer = await _customerRepository.UpdateAsync(customer).ConfigureAwait(false);
                if (newCustomer != null)
                {
                    return Ok();
                }
            }

            return NotFound();
        }
        
        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await (await _customerRepository.GetAllAsync().ConfigureAwait(false))
                .AsQueryable()
                .Include(x => x.PromoCodes)
                .FirstOrDefaultAsync(x => x.Id == id)
                .ConfigureAwait(false);
            
            if (customer != null)
            {
                foreach (var promoCode in customer.PromoCodes)
                {
                    await _promoCodeRepository.RemoveAsync(promoCode.Id).ConfigureAwait(false);
                }

                if (await _customerRepository.RemoveAsync(customer.Id).ConfigureAwait(false))
                {
                    return Ok();
                };
            }

            return NotFound();
        }
    }
}