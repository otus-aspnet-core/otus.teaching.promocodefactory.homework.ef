﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;


        public PromocodesController(
            IRepository<PromoCode> promoCodeRepository,
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync().ConfigureAwait(false);
            if (promoCodes?.Any() == true)
            {
                return promoCodes
                    .Select(x => new PromoCodeShortResponse(x))
                    .ToList();
            }

            return NotFound();
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            if (!string.IsNullOrEmpty(request?.PromoCode))
            {
                var preference = await (await _preferenceRepository.GetAllAsync().ConfigureAwait(false))
                    .AsQueryable()
                    .Include(x => x.CustomerPreferences)
                        .ThenInclude(x => x.Customer)
                            .ThenInclude(x => x.PromoCodes)
                    .FirstOrDefaultAsync(x => x.Name == request.Preference)
                    .ConfigureAwait(false);
                
                if (preference != null)
                {
                    var promoCode = await _promoCodeRepository.CreateAsync(request.ToPromoCode(preference)).ConfigureAwait(false);
                    if (promoCode != null)
                    {
                        if (preference.CustomerPreferences?.Any() == true)
                        {
                            foreach (var customerPreference in preference.CustomerPreferences)
                            {
                                if (customerPreference.Customer != null)
                                {
                                    customerPreference.Customer.AddPromoCode(promoCode);
                                    
                                    await _customerRepository.UpdateAsync(customerPreference.Customer).ConfigureAwait(false);
                                }
                            }

                            return Ok();
                        }
                    }
                }
            }

            return NotFound();
        }
    }
}