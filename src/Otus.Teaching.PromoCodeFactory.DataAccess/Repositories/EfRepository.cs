﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Models;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> 
        : IRepository<T> where T : BaseEntity
    {
        private readonly DatabaseContext _db;


        public EfRepository(DatabaseContext db)
        {
            _db = db;
        }


        public Task<IEnumerable<T>> GetAllAsync()
        {
            //return await _db.Set<T>().ToArrayAsync().ConfigureAwait(false);
            return Task.FromResult((IEnumerable<T>)_db.Set<T>());
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == id).ConfigureAwait(false);
        }

        public async Task<T> UpdateAsync(T entity)
        {
            if (entity != null)
            {
                var oldEntity = await this.GetByIdAsync(entity.Id).ConfigureAwait(false);
                if (oldEntity != null)
                {
                    _db.Entry(oldEntity).CurrentValues.SetValues(entity);
                    await _db.SaveChangesAsync().ConfigureAwait(false);

                    return await this.GetByIdAsync(entity.Id).ConfigureAwait(false);
                }
            }

            return null;
        }

        public async Task<T> CreateAsync(T entity)
        {
            if (entity != null)
            {
                await _db.Set<T>().AddAsync(entity).ConfigureAwait(false);
                await _db.SaveChangesAsync().ConfigureAwait(false);

                return await this.GetByIdAsync(entity.Id).ConfigureAwait(false);
            }

            return null;
        }

        public async Task<bool> RemoveAsync(Guid id)
        {
            var entity = await this.GetByIdAsync(id).ConfigureAwait(false);
            if (entity != null)
            {
                _db.Set<T>().Remove(entity);

                await _db.SaveChangesAsync().ConfigureAwait(false);

                return true;
            }

            return false;
        }
    }
}