﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Models
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public DbSet<Role> Roles { get; set; }


        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerPreference>()
                .HasKey(x => new {x.CustomerId, x.PreferenceId});

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(x => x.Customer)
                .WithMany(x => x.CustomerPreferences)
                .HasForeignKey(x => x.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(x => x.Preference)
                .WithMany(x => x.CustomerPreferences)
                .HasForeignKey(x => x.PreferenceId);
        }

        public void Init()
        {
            // Добавление миграции по чужому Startup.cs
            // dotnet ef migrations add InitialCreate --startup-project ..\Otus.Teaching.PromoCodeFactory.WebHost\Otus.Teaching.PromoCodeFactory.WebHost.csproj

            //Database.EnsureDeleted();

            if (Database.EnsureCreated() || !this.Customers.Any())
            {
                this.SeedDatabaseContext();
            }
        }
    }
}