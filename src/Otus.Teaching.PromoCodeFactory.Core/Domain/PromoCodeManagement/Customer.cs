﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        : BaseEntity
    {
        [MaxLength(100)]
        public string FirstName { get; set; }
        
        [MaxLength(100)]
        public string LastName { get; set; }

        [NotMapped]
        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(100)]
        public string Email { get; set; }

        public int Age { get; set; }

        //TODO: Списки Preferences и Promocodes
        
        public ICollection<CustomerPreference> CustomerPreferences { get; set; }
        
        public ICollection<PromoCode> PromoCodes { get; set; }


        public Customer SetNewId(Guid? id = default)
        {
            this.Id = id ?? Guid.NewGuid();

            return this;
        }

        public Customer SetPreferences(ICollection<Preference> preferences)
        {
            this.CustomerPreferences = preferences?
                .Select(x => new CustomerPreference()
                {
                    CustomerId = this.Id,
                    PreferenceId = x.Id,
                    Preference = x
                })
                .ToArray() ?? this.CustomerPreferences;

            return this;
        }

        public Customer AddPromoCode(PromoCode promoCode)
        {
            this.PromoCodes = this.PromoCodes ?? new List<PromoCode>();

            if (promoCode != null)
            {
                this.PromoCodes.Add(promoCode);
            }

            return this;
        }
    }
}