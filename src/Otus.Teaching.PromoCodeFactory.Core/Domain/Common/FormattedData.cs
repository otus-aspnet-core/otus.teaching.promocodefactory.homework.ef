﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Common
{
    public static class FormattedData
    {
        public static string ToString(DateTime dateTime) => dateTime.ToString("dd.MM.yyyy");
    }
}